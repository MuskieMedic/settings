# FireDragon settings

I encourage users to find **their own setup** and to use our default configuration as something to build on top of.
- -> `~/.firedragon/firedragon.overrides.cfg`

This repository benefits from the knowledge and research provided by [arkenfox](https://github.com/arkenfox), their documentation was vital to this revamp, so special thanks to their project.
We do not use arkenfox's user.js but we try to keep up with it, and we also consider it a great resource for users who want to find their own setup.

Some of the older prefs in this project are taken from [pyllyukko](https://github.com/pyllyukko/user.js/) and many more were investigated on [bugzilla](https://bugzilla.mozilla.org/home).

Differences from LibreWolf:

- Enhanced KDE integration due to OpenSUSE patches (also using kfiredragonhelper)
- Uses system own libraries wherever possible
- [Proton UI rework](https://wiki.mozilla.org/Firefox/Proton) enabled
- Compatible with Wayland, read the pinned post 8 for instruction
- Enabled [PGO](https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Build_Instructions/Building_with_Profile-Guided_Optimization)
- Disabled megabar
- [Searx](https://searx.garudalinux.org/) & [Whoogle](https://search.garudalinux.org/) search engines added, with the possibility to run locally with fitting optdepends installed
- The default search engine is Garudas [searX instance](https://searx.garudalinux.org)
- [Canvasblocker](https://addons.mozilla.org/en-US/firefox/addon/canvasblocker/), [Dark Reader](https:/]/addons.mozilla.org/en-US/firefox/addon/darkreader/), [ClearURLs](https://addons.mozilla.org/de/firefox/addon/clearurls/) & [Tabliss](https://addons.mozilla.org/en-US/firefox/addon/tabliss/) addons added 
- DNS-over-HTTPS using [Quad9](https://quad9.net) servers to unblock censorship by the internet provider
- Sweet theme added
- Custom, dr460nized branding :dragon:
- Will keep in sync with both Librewolfs & Plasmafox changes
- Presets for both profile-sync-daemon (which Garuda ships by default) & Firejail available
- Builds available at [Chaotic-AUR](https://chaotic.cx)
- Icons & artwork by [SGS](https://gitlab.com/sgse) & [zoeronen](https://gitlab.com/zoeronen)
- Tabliss setup from the screenshot can be imported from tabliss.json

Differences of the Nightly build:

- Built off Firefox Nightly instead of stable
- [Proton UI rework](https://wiki.mozilla.org/Firefox/Proton) enabled
- No patches available for Nightly means no KDE integration & appmenu support
- Will keep in sync with both Librewolfs & [vnepogodins](https://aur.archlinux.org/account/vnepogodin) changes

Builds for Arch based distros available at [Chaotic-AUR](https://chaotic.cx).

<img src=https://gitlab.com/dr460nf1r3/dragonwolf-settings/-/raw/master/home.png/>
<img src=https://gitlab.com/dr460nf1r3/dragonwolf-settings/-/raw/master/about.png/>


Credits go to Mozilla, Arch Linux, and:

- The [Librewolf](https://librewolf-community.gitlab.io/) project
- torvic9 & his [Plasmafox](https://github.com/torvic9/plasmafox)
- [vnepogodin](https://aur.archlinux.org/account/vnepogodin)
- [SGS](https://gitlab.com/sgse)
